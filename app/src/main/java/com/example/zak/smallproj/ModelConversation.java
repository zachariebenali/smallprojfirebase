package com.example.zak.smallproj;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.Serializable;

/**
 * Created by Zak on 13/05/2015.
 */
public class ModelConversation implements Serializable {

    private String Nom;
    private String Contenu;


    public ModelConversation(String nom, String contenu) {
        this.Nom = nom;
        this.Contenu = contenu;
    }


    public String getNom() {

        return Nom;
    }

    public String getContenu() {

        return Contenu;
    }

    public void setContenu(String contenu) {

        this.Contenu = contenu;
    }

}