package com.example.zak.smallproj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.app.Application;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class ConversationHandler extends Application{

    /** Instance unique pr�-initialis�e */
    private static ConversationHandler INSTANCE = new ConversationHandler();
    private ListeConversationNotifier controller;
    private Map<String,Conversation> conversations = new HashMap<String,Conversation>();
    private Context baseContext;

    /** Point d'acc�s pour l'instance unique du singleton */
    public static ConversationHandler getInstance() {
        return INSTANCE;
    }

    public Conversation getConversation(String key) {
        return conversations.get(key);
    }

    public void setConversation(String key, Conversation conversation) {
        conversations.put(key, conversation);
    }

    public int getConversationsArraylistSize() {

        return conversations.size();
    }

    public Map<String,Conversation> getConversations(){
        return conversations;
    }

    public ArrayList toArraylist(){
        ArrayList<String> keys = new ArrayList<String>();
        for(String key : conversations.keySet()){
            keys.add(key);
        }
        return keys;
    }

    public void setController(ListeConversationNotifier controller) {
        this.controller = controller;
    }

    public void startListener() {
        Constants.CONVERS_REF.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Iterator<DataSnapshot> it = snapshot.getChildren().iterator();
                while (it.hasNext()) {
                    String key = it.next().getKey();
                    Conversation modconv = new Conversation((String) snapshot.child(key).child("nom").getValue(), (String) snapshot.child(key).child("contenu").getValue());
                    setConversation(key, modconv);
                }
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, toArraylist());
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, toArraylist());
                controller.listeConversationContentChanged(adapter);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    public void setBaseContext(Context baseContext) {
        this.baseContext = baseContext;
    }


    @Override
    public Context getBaseContext() {
        return baseContext;
    }
}