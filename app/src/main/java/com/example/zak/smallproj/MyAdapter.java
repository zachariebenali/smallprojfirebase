package com.example.zak.smallproj;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Zak on 01/06/2015.
 */
public class MyAdapter extends BaseAdapter {
    private final ArrayList mData;

    public MyAdapter(Map<String, Conversation> map) {
        mData = new ArrayList();
        mData.addAll(map.entrySet());
    }

    public ArrayList getArraylist(){
        return mData;
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }


}