package com.example.zak.smallproj;

import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.Serializable;

public class Conversation implements Serializable {

    private ConversationNotifier controller;
    private String nom;
    private String contenu;


    public Conversation(String nom, String contenu) {
        this.nom = nom;
        this.contenu = contenu;

    }

    public void setController(ConversationNotifier controller){
        this.controller = controller;
    }

    public void startListener(){

        //listener sur la conversation firebase
        Constants.CONVERS_REF.child(getNom()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                controller.conversationContentChanged(getContenu());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });

    }

    public String getNom() {

        return nom;
    }

    public String getContenu() {

        return contenu;
    }

    public void setContenu(String contenu) {

        this.contenu = contenu;
        Constants.CONVERS_REF.child(getNom()).child("contenu").setValue(contenu);

    }

}