package com.example.zak.smallproj;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.client.Firebase;


public class ConversationActivity extends ActionBarActivity implements ConversationNotifier {

    private TextView nomConversView;
    private TextView contenuConversView;
    private EditText contenuConversBox;
    private String nameConversation;
    private Conversation currentConversation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        Firebase.setAndroidContext(this);

        //initialisation des View
        nomConversView = (TextView) findViewById(R.id.nameView);
        contenuConversView = (TextView) findViewById(R.id.contenuView);
        contenuConversBox = (EditText) findViewById(R.id.editText);

        //recuperation du nom de la conversation cliquee depuis la premiere activite
        nameConversation= getIntent().getExtras().getString("nomConversation");

        final ConversationHandler conversationHandler = ConversationHandler.getInstance();

        //r�cupere la conversation associ�e
        currentConversation = conversationHandler.getConversation(nameConversation);
        currentConversation.setController(this);
        currentConversation.startListener();

        //remplit nomConversView avec le nom de la conversation et contenuConversBox avec le contenu de la conversation
        nomConversView.setText(nameConversation);
        contenuConversBox.setText(currentConversation.getContenu());

        //listener sur contenuConversBox
        //contenuConversBox.addTextChangedListener(currentConversation.function())

        contenuConversBox.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //quand on modifie l'editText on modifie le contenu sur la base
                currentConversation.setContenu(s.toString());
            }

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //ConversationNotifier
    @Override
    public void conversationContentChanged(String content) {

        contenuConversView.setText(content);
    }
}
