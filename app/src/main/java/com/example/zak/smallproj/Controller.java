package com.example.zak.smallproj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Application;

public class Controller extends Application{

    private Map<String,ModelConversation> conversations = new HashMap<String,ModelConversation>();

    public ModelConversation getConversation(String key) {
        return conversations.get(key);
    }

    public void setConversation(String key, ModelConversation conversation) {
        conversations.put(key, conversation);
    }

    public int getConversationsArraylistSize() {

        return conversations.size();
    }

    public Map<String,ModelConversation> getConversations(){
        return conversations;
    }

    public ArrayList toArraylist(){
        ArrayList<String> keys = new ArrayList<String>();
        for(String key : conversations.keySet()){
            keys.add(key);
        }
        return keys;
    }

}