package com.example.zak.smallproj;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.Firebase;


public class ListConvActivity extends ListActivity implements ListeConversationNotifier {

    private ListeConversationNotifier listeController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_list_conv);

        ConversationHandler conversationHandler = ConversationHandler.getInstance();
        conversationHandler.setBaseContext(getBaseContext());
        conversationHandler.setController(this);
        conversationHandler.startListener();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        //r�cup�re et envoie le nom de la conversation a la nouvelle activit�
        Intent intent = new Intent(this, ConversationActivity.class);
        TextView tv = (TextView)v;
        intent.putExtra("nomConversation", tv.getText());
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_conv, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void listeConversationContentChanged(ArrayAdapter<String> adapter) {
        //affiche la liste des noms de conversation dans la listView
        setListAdapter(adapter);
    }
}
