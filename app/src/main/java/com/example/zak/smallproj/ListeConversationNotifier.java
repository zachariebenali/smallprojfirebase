package com.example.zak.smallproj;

import android.widget.ArrayAdapter;

/**
 * Created by Zak on 02/06/2015.
 */
public interface ListeConversationNotifier {

    public void listeConversationContentChanged(ArrayAdapter<String> content);

}