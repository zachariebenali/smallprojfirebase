package com.example.zak.smallproj;

/**
 * Created by Zak on 13/05/2015.
 */
public interface ConversationNotifier{

    public void conversationContentChanged(String content);


}
